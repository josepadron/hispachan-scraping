const request = require("request");
const cheerio = require("cheerio");
const fs = require("fs");

function recursiveDownload (urlArray, nameArray, i) {
    if (i < urlArray.length) {
        if(!fs.existsSync("img/")){
            fs.mkdirSync("img/");
        }

        if (!fs.existsSync(`img/${tablon}`)){
            fs.mkdirSync(`img/${tablon}/`);
        }

        if (!fs.existsSync(`img/${tablon}/${hilo}`)){
            fs.mkdirSync(`img/${tablon}/${hilo}`);
        }

        if (!fs.existsSync(`img/${tablon}/${hilo}/${nameArray[i]}`)){
            console.log(`[${i+1}/${urlArray.length}] - ${nameArray[i]} - descargando`);

            request.get(urlArray[i])                                                                                  
                .on('error', function(err) {console.log(err)} )                                                   
                .pipe(fs.createWriteStream(`img/${tablon}/${hilo}/${nameArray[i]}`))                                                                 
                .on('close', function () { recursiveDownload (urlArray, nameArray, i+1); });
        }else {
            console.log(`[${i+1}/${urlArray.length}] - ${nameArray[i]} - ya existe`);
            recursiveDownload (urlArray, nameArray, i+1);
        }        
        
    }
}

let url = process.argv.slice(2)[0];

if(/(http[s]?:\/\/)?([^\/\s]+\/)(.*)/.test(url) == false) {
    console.error("Error: no hay url valida, url debe ser: https://www.hispachan.org/XX/res/XXX.html#XXX");
    process.exit(0);
}

const partes = url.split("/");

const tablon = partes[3];
const hilo = partes[5].split(".")[0];

if(!tablon || !hilo) {
    console.error("Error al obtener tablon o hilo");
    process.exit(0);
}

var src = [];
var names = [];

console.log("Obteniendo HTML");

request(url, (error, resp, body) => {
    if (!error && resp.statusCode === 200) {
        console.log("Obteniendo href");

        let $ = cheerio.load(body);

        // Obtiene el archivo del post que empezó el hilo
        $(".thread > span > a").each((i, el) => {
            let href = el.attribs.href;
            names.push(href.substring(href.lastIndexOf("/")+1));
            src.push(href);
        });

        // Obtiene el resto de archivos
        $(".filenamereply > a").each((i, el) => {
            let href = el.attribs.href;
            names.push(href.substring(href.lastIndexOf("/")+1));
            src.push(href);
        });

        if(src.length == 0) {
            console.error("No hay imagenes/videos para descargar");
            process.exit(0);
        }

        recursiveDownload(src, names, 0);
    }
});